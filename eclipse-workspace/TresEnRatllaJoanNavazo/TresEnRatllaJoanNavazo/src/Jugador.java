import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {
	public static Scanner reader = new Scanner(System.in);

	/*
	 * <h2>DefinirJugadors</h2> Demanar el Nom dels 2 jugadors, comparo els dos per
	 * evitar iguals.
	 * 
	 * @param: ArrayList<String> Jugadors
	 * 
	 * @return: ArrayList<String> Jugadors
	 */

	public static ArrayList<String> DefinirJugadors(ArrayList<String> Jugadors) {
		do {
			reader.nextLine();
			System.out.println("Introdueix el nom del jugador1: ");
			Jugadors.set(0, reader.nextLine());
			System.out.println("Introdueix el nom del jugador2: Ha de ser diferent al del jugador 1");
			Jugadors.set(1, reader.nextLine());
		} while (Jugadors.get(0).equals(Jugadors.get(1)));
		return Jugadors;
	}

}
