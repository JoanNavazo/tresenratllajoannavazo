import java.util.Scanner;
import java.util.ArrayList;

/**
 * <h2>TresEnRatlla</h2>
 * @author JoanNavazo
 * @version 1
 */

public class TresEnRatlla {
	public static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {

		int tauler[][] = new int[3][3];
		int columna = 0;
		int fila = 0;
		int torn = 0;
		int opcio = 0;
		int guanyador = 0;
		boolean definits = false;
		boolean marcada = false;
		ArrayList<String> Jugadors = new ArrayList<String>();
		ArrayList<Integer> victories = new ArrayList<Integer>();

		Jugadors.add("");
		Jugadors.add("");
		victories.add(0);
		victories.add(0);
		do {
			Jugar.MostrarMenu();
			opcio = Jugar.DemanarOpcio();
			switch (opcio) {
			case 1:
				Ajuda.MostrarAjuda();
				break;
			case 2:
				Jugadors = Jugador.DefinirJugadors(Jugadors);
				victories.set(0, 0);
				victories.set(1, 0);
				tauler = Jugar.ReiniciarTauler(tauler);
				definits = true;
				break;
			case 3:
				if (definits == false) {
					System.out.println("Introdueix primer els jugadors! Opci� 2");
				} else {
					Jugar.MostrarTauler(tauler);
					do {
						columna = Jugar.EscollirColumna();
						fila = Jugar.EscollirFila();
						marcada = Jugar.ComprovarCasella(fila, columna, torn, tauler);
					} while (marcada == true);
					guanyador = Jugar.ComprovarGuanyador(guanyador, tauler);
					if (guanyador == 1) {
						guanyador = 0;
						System.out.println("Jugador " + Jugadors.get(torn) + " has guanyat!");
						victories.set(torn, +1);
						tauler = Jugar.ReiniciarTauler(tauler);
					} else if (guanyador == 2) {
						guanyador = 0;
						System.out.println("Jugador " + Jugadors.get(1) + " has guanyat!");
						victories.set(1, +1);
						tauler = Jugar.ReiniciarTauler(tauler);
					} else if (guanyador == 3) {
						System.out.println("Taules!");
						tauler = Jugar.ReiniciarTauler(tauler);
					}
					if (torn == 0) {
						torn = 1;
						System.out.println(Jugadors.get(1) + " �s el teu torn.");
					} else if (torn == 1) {
						torn = 0;
						System.out.println(Jugadors.get(0) + " �s el teu torn.");
					}
				}
				break;
			case 4:
				if (definits == false) {
					System.out.println("Introdueix primer els jugadors! Opci� 2");
				} else {
					System.out.println(Jugadors.get(0) + " t� " + victories.get(0) + " partides guanyades");
					System.out.println(Jugadors.get(1) + " t� " + victories.get(1) + " partides guanyades");
				}
				break;
			case 0:
				System.out.println("Fi");
				break;
			default:
				System.out.println("Opci� no valida!");
			}
		} while (opcio != 0);
	}

}
