import java.util.Scanner;

public class Jugar {
	public static Scanner reader = new Scanner(System.in);

	/*
	 * <h2>MostrarMen�</h2> Mostrara el men� al jugador.
	 */
	public static void MostrarMenu() {
		System.out.println("");
		System.out.println("Men� d'opcions:");
		System.out.println("1 = Mostrar Ajuda.");
		System.out.println("2 = Definir jugadors.");
		System.out.println("3 = Jugar partida.");
		System.out.println("4 = Estat de la partida.");
		System.out.println("0 = Sortir.");
	}

	/*
	 * /* <h2>DemanarOpcio</h2> Aquest m�tode serveix per a demanar l�opci� i el
	 * try{ }catch far� que una lletra no avorti el programa
	 * 
	 * @return: opcio
	 */
	public static int DemanarOpcio() {
		int op = 0;
		try {
			System.out.println("Escull una opci�.");
			op = reader.nextInt();
		} catch (Exception e) {
			System.out.println("Ha de ser un nombre enter!!!!!");
			reader.nextLine();
		}
		return op;
	}

	/*
	 * <h2>MostrarTauler</h2> Mostrara la situaci� actual del tauler
	 * 
	 * @param: int[][] tauler
	 */
	public static void MostrarTauler(int[][] tauler) {
		for (int fila = 0; fila < 3; fila++) {
			for (int columna = 0; columna < 3; columna++) {
				if (tauler[fila][columna] == 0) {
					System.out.print("| |");
				} else if (tauler[fila][columna] == 1) {
					System.out.print("|X|");
				} else {
					System.out.print("|O|");
				}
			}
			System.out.println("");
		}
	}

	/*
	 * <h2>EscollirColumna i EscollirFila</h2> Permeten al jugador escolliron
	 * col�locar la marca
	 * 
	 * @return: columna
	 * 
	 * @return: fila
	 */

	public static int EscollirColumna() {
		int columna = 0;
		do {
			try {
				System.out.println("Introdueix la columna on posar la fitxa: entre 0 i 2");
				columna = reader.nextInt();
			} catch (Exception e) {
				System.out.println("Ha de ser un nombre enter!!!!!");
				reader.nextLine();
			}
		} while (columna < 0 || columna > 2);
		return columna;
	}

	public static int EscollirFila() {
		int fila = 0;
		do {
			try {
				System.out.println("Introdueix la fila on posar la fitxa: entre 0 i 2");
				fila = reader.nextInt();
			} catch (Exception e) {
				System.out.println("Ha de ser un nombre enter!!!!!");
				reader.nextLine();
			}
		} while (fila < 0 || fila > 2);
		return fila;
	}

	/*
	 * ComprovarCasella Comprovar� si la casella est� plena i en cas de no ser-ho
	 * l'omple amb un 1 si �s el torn del primer jugador i un dos pel segon
	 * 
	 * @param: int fila, int columna, int torn, int[][] tauler
	 * 
	 * @return: marcada
	 */

	public static boolean ComprovarCasella(int fila, int columna, int torn, int[][] tauler) {
		boolean marcada = false;
		if (tauler[fila][columna] == 0) {
			if (torn == 0) {
				tauler[fila][columna] = 1;
			}
			if (torn == 1) {
				tauler[fila][columna] = 2;
			}
		} else {
			System.out.println("Aquesta casilla �s troba marcada, introdueix una de nova");
			marcada = true;
		}
		return marcada;
	}

	/*
	 * <h2>ReiniciarTauler</h2> Neteja totes les caselles del tauler
	 */

	public static int[][] ReiniciarTauler(int[][] tauler) {
		for (int fila = 0; fila < 3; fila++) {
			for (int columna = 0; columna < 3; columna++) {
				tauler[fila][columna] = 0;
			}
		}
		return tauler;
	}

	/*
	 * <h2>ComprovarGuanyador</h2> El doble bucle de while serveix per a comprovar
	 * si falta alguna casella per completar i en cas contrari si no hi hagues cap
	 * guanyador declararia taules.
	 * 
	 * @return: guanyador
	 */

	public static int ComprovarGuanyador(int guanyador, int[][] tauler) {
		int fila = 0;
		int columna = 0;
		boolean incomplet = false;

		while (fila < 3) {
			while (columna < 3) {
				if (tauler[fila][columna] == 0) {
					incomplet = true;
				}
				columna++;
			}
			fila++;
			columna = 0;
		}

		if (tauler[0][0] == 1 && tauler[0][1] == 1 && tauler[0][2] == 1
				|| tauler[1][0] == 1 && tauler[1][1] == 1 && tauler[1][2] == 1
				|| tauler[2][0] == 1 && tauler[2][1] == 1 && tauler[2][2] == 1
				|| tauler[0][0] == 1 && tauler[1][0] == 1 && tauler[2][0] == 1
				|| tauler[0][1] == 1 && tauler[1][1] == 1 && tauler[2][1] == 1
				|| tauler[0][2] == 1 && tauler[1][2] == 1 && tauler[2][2] == 1
				|| tauler[0][0] == 1 && tauler[1][1] == 1 && tauler[2][2] == 1
				|| tauler[0][2] == 1 && tauler[1][1] == 1 && tauler[2][0] == 1) {
			guanyador = 1;
		} else if (tauler[0][0] == 2 && tauler[0][1] == 2 && tauler[0][2] == 2
				|| tauler[1][0] == 2 && tauler[1][1] == 2 && tauler[1][2] == 2
				|| tauler[2][0] == 2 && tauler[2][1] == 2 && tauler[2][2] == 2
				|| tauler[0][0] == 2 && tauler[1][0] == 2 && tauler[2][0] == 2
				|| tauler[0][1] == 2 && tauler[1][1] == 2 && tauler[2][1] == 2
				|| tauler[0][2] == 2 && tauler[1][2] == 2 && tauler[2][2] == 2
				|| tauler[0][0] == 2 && tauler[1][1] == 2 && tauler[2][2] == 2
				|| tauler[0][2] == 2 && tauler[1][1] == 2 && tauler[2][0] == 2) {
			guanyador = 2;
		} else if (incomplet == false) {
			guanyador = 3;
		}

		return guanyador;
	}
}
